(comment
  "MIT License

Copyright (c) 2022 Andrey Listopadov

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the “Software”), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.")

(local debugger-id {})

(local debug {})

(fn with-break-point [expr {: id : viewed}]
  (let [{: col : endcol : line} (ast-source expr)]
    `(let [res# (#(doto [$...] (tset :n (select :# $...))) ,expr)]
       (coroutine.yield {:expr ,(or viewed (view expr {:one-line? true}))
                         :res res#
                         :debugger-id ,(tostring debugger-id)
                         :id ,(or id 0)
                         :start-col ,col
                         :end-col ,endcol
                         :line ,line}))))

(fn debug.let [form debug-form id]
  (let [viewed (view form {:one-line? true})
        bindings (. form 2)]
    (for [i 2 (length bindings) 2]
      (tset bindings i (debug-form (. bindings i) (- id 2))))
    (for [i 3 (length form)]
      (tset form i (debug-form (. form i) (- id 1))))
    (with-break-point form {: id : viewed})))

(fn debug.local [form debug-form id]
  (let [viewed (view form {:one-line? true})]
    (tset form 3 (debug-form (. form 3) (- id 1)))
    (with-break-point form {: id : viewed})))

(fn debug.do [form debug-form id]
  (let [viewed (view form {:one-line? true})]
    (for [i 2 (length form)]
      (tset form i (debug-form (. form i) (- id 1))))
    (with-break-point form {: id : viewed})))

(fn debug.match [form debug-form id]
  (let [viewed (view form {:one-line? true})]
    (tset form 2 (debug-form (. form 2) id))
    (for [i 4 (length form) 2]
      (tset form i (debug-form (. form i) id)))
    (with-break-point form {: id : viewed})))

(fn debug.each [form debug-form id]
  (let [viewed (view form {:one-line? true})]
    (for [i 3 (length form)]
      (tset form i (debug-form (. form i) (- id 1))))
    (with-break-point form {: id : viewed})))

(fn debug.sequence [form debug-form id]
  (let [viewed (view form {:one-line? true})]
    (each [k v (ipairs form)]
      (tset form k (debug-form v (- id 1))))
    (with-break-point form {: id : viewed})))

(fn debug.table [form debug-form id]
  (let [viewed (view form {:one-line? true})]
    (each [k v (pairs form)]
      (tset form k (debug-form v (- id 1))))
    (with-break-point form {: id : viewed})))

(fn debug-form [form id]
  "Recursively injects breakpoints on all forms meaningfully."
  (if (list? form)
      (let [debugger (match (tostring (. form 1))
                       (where (or :let :with-open)) debug.let
                       (where (or :local :var :global)) debug.local
                       (where (or :do :if :when :and :or :not :while)) debug.do
                       :match debug.match
                       (where (or :each :collect :icollect :accumulate :fcollect :for))
                       debug.each
                       _ debug.sequence)]
        (debugger form debug-form id))
      (sequence? form)
      (debug.sequence form debug-form id)
      (table? form)
      (debug.table form debug-form id)
      (sym? form)
      (let [specials (. (get-scope) :specials)]
        (if (not (. specials (tostring form)))
            (with-break-point form {: id})
            form))
      (with-break-point form {: id})
      form))

(fn with-debugger [form]
  "Injects breakpoints in the form, and starts an interactive debugger prmpt loop."
  (let [id 1000]
    `(let [thunk# (coroutine.create (fn [] ,(debug-form form id)))
           unpack# (or table.unpack _G.unpack)
           tostring# (match (pcall require :fennel)
                       (true {:view view#}) view#
                       ,(sym :_) tostring)]
       (var (continue# done# res# start# out# curr-id# out-id#)
         (values false false {:n 0} true false nil 0))
       (print "Enterred step debugger. Commands:")
       (print "c: continue, s/n: step over, o: step out, p: pprint value, q: exit")
       (while (and (not done#) (= :suspended (coroutine.status thunk#)))
         (match (if (or continue# start# (and curr-id# (< curr-id# out-id#)))
                    (coroutine.resume thunk# (unpack# res# 1 res#.n))
                    (match (do
                             (io.stdout:write "debugger>> ")
                             (io.stdin:read))
                      :c (set continue# true)
                      :o (set (out# out-id#) (values true (+ curr-id# 1)))
                      (where (or :s :n))
                      (coroutine.resume thunk# (unpack# res# 1 res#.n))
                      :p (do
                           (for [i# 1 res#.n]
                             (print (tostring# (. res# i#))))
                           res#)
                      :q (error :interrupted)))
           (true {:expr expr# :res result#
                  :debugger-id ,(tostring debugger-id)
                  :id id# &as data#})
           (let [formatted# (table.concat (fcollect [i# 1 result#.n]
                                            (let [s# (tostring# (. result# i#) {:one-line? true})]
                                              (if (> (length s#) 20)
                                                  (.. (string.sub s# 1 17) "..." (string.sub s# -3))
                                                  s#)))
                                          ", ")]
             (set curr-id# id#)
             (when (and out# (>= id# out-id#))
               (set out# false))
             (when (and (not out#) (not continue#))
               (print (string.format "%s:%s:%s %s => %s"
                                     data#.line data#.start-col data#.end-col
                                     expr# (if (= formatted# "") "nil" formatted#))))
             (set res# result#))
           (true ,(sym :_)) (set done# true)
           (false err#) (error err#))
         (set start# false))
       (unpack# res# 1 res#.n))))

(comment
  (require-macros :with-debugger)
  (with-debugger
   (let [a (+ 1 2)
         b (+ a 3)]
     (fcollect [i 1 4]
       (if (= 0 (% i 2))
           (+ a b i)
           (when (= 0 (% (math.random 100) 2))
             :foo)))))
  )

{: with-debugger}
